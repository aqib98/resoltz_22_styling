/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput

} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';

const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

import SplashScreen from 'react-native-smart-splash-screen'
import styles from './styles.js'



export default class SignUp extends Component<{}> {
  constructor(props) {
  super(props);
  this.state = {
    width:0,
    height : 0
  };
}
componentDidMount () {


}

  onPressChevronLeft = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Home',
      params: {'Home':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPressLogin = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'UpcomingWorkouts',
      params: {'UpcomingWorkouts':'yes'},
      })
    navigate.dispatch(navigateAction)
  }



// <View>
//     <Icon name="chevron-left" size={30} color="#900" style={{marginLeft:16,marginTop:16,position:'absolute'}} />
// </View>
  render() {
    const {navigation} = this.props
    const resizeMode = 'center';
    const text = 'I am some centered text';
    const myIcon = (<Icon name="rocket" size={30} color="#900" />)
    console.log(styles)
    return (


            <View style={styles.mainBody}>
              <View style={styles.chevron_left_icon}>
                <TouchableOpacity onPress={()=>this.onPressChevronLeft(navigation )}>
                  <Icon name="chevron-left" size={25} color="#FF7E00"   />
                </TouchableOpacity>
              </View>
              <View style={styles.header}>
                    <Text style={styles.topSignupTxt}>
                      School sign up
                    </Text>
              </View>

               <View>
                <Text style={styles.wel_scholl_heading}>Welcome to the ResoltZ app!</Text>
               </View>
               <View style={styles.wel_scholl_cont_mas}>
               <Text style={styles.wel_scholl_cont}>
                   Hi! We are so excited to have you join the resoltZ community. You now have the power to achieve your
                   fitness goals, on your own terms.
               </Text>
                <Text style={styles.wel_scholl_cont2}>
                  Let us get started!
                </Text>
               </View>

               <TouchableOpacity onPress={()=>this.onPressLogin(navigation )} style={styles.touch_align1}>
               <View style={styles.circle1_mas1}>
                 <View style={styles.circle1_mas}>
                    <Text style={styles.circle1_txt}>1</Text>
                    <Text style={styles.user_info_btn1}>User info</Text>
                    <Icon name="chevron-right" size={25} style={styles.user_info_Arrow}   />
                 </View>
               </View>
               </TouchableOpacity>
               <TouchableOpacity onPress={()=>this.onPressLogin(navigation )} style={styles.touch_align2}>
               <View style={styles.circle1_mas1}>
                 <View style={styles.circle1_mas}>
                    <Text style={styles.circle1_txt}>1</Text>
                    <Text style={styles.user_info_btn1}>Profile details</Text>
                    <Icon name="chevron-right" size={25} style={styles.user_info_Arrow}   />
                 </View>
               </View>
               </TouchableOpacity>
               <TouchableOpacity onPress={()=>this.onPressLogin(navigation )} style={styles.touch_align2}>
               <View style={styles.circle1_mas1}>
                 <View style={styles.circle1_mas}>
                    <Text style={styles.circle1_txt}>1</Text>
                    <Text style={styles.user_info_btn1}>Pick class info</Text>
                    <Icon name="chevron-right" size={25} style={styles.user_info_Arrow}   />
                 </View>
               </View>
               </TouchableOpacity>




              <View style={styles.footer}>

                  <TouchableOpacity onPress={()=>this.onPressLogin(navigation )}>
                    <View style={styles.save_view}>
                        <Text style={styles.save_btnTxt}>Save</Text>
                    </View>

                    <Image
                      style={styles.save_btnImg}
                      source={{ uri: 'buttonimg' }}
                    />

                  </TouchableOpacity>
                </View>

                  </View>



    );
  }
}
