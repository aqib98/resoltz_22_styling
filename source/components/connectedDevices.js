/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
Platform,
StyleSheet,
Text,
View,
Switch,
Image,
PixelRatio,
KeyboardAvoidingView,
ScrollView,
TouchableHighlight,
TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination,ParallaxImage } from 'react-native-snap-carousel';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'

import SplashScreen from 'react-native-smart-splash-screen'

import styles from './styles.js'
import { ENTRIES1 } from './entries';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideHeight = viewportHeight * 0.4;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;


export default class ConnectedDevices extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      width:0,
      height : 0,
      slider1Ref :null,
      slider1ActiveSlide: 1,
    };
  }


render() {
  const {navigation} = this.props
  const resizeMode = 'center';
  const text = 'I am some centered text';
  return (

    <View style={styles.mainBody}>
      <View style={styles.chevron_left_icon}>
        <TouchableOpacity onPress={()=>this.onPressChevronLeft(navigation )}>
          <Icon name="chevron-left" size={25} color="#FF7E00"   />
        </TouchableOpacity>
      </View>
      <View style={styles.header}>
            <Text style={styles.topSignupTxt}>
              Connect device
            </Text>
      </View>

      <ScrollView
        style={{
          marginTop : 20,
          marginBottom:0,
          paddingBottom:60,

          width:Dimensions.get('window').width
        }}
      >
      <View>
            <View><Text style={styles.Searching1}>Searching...</Text></View>
            <View>
                <Text style={styles.connectr_name}>Sophie Apple Watch</Text>
                <View style={styles.connected_txt1Mas}>
                    <Text style={styles.connected_txt1}>Connected</Text>
                    <View style = {styles.container}>
                       <Switch
                          onValueChange = {()=>{
                            console.log('checked')
                          }}
                          value = {true}/>
                    </View>
                </View>
            </View>
            <View>
                <Text style={styles.connectr_name}>Sophie Apple Health</Text>
                <View style={styles.connected_txt1Mas}>
                    <Text style={styles.connected_txt1}>Not Connected</Text>
                    <View style = {styles.container}>
                       <Switch
                          onValueChange = {()=>{
                            console.log('checked')
                          }}
                          value = {false}/>
                    </View>
                </View>
            </View>
            <View>
                <Text style={styles.connectr_name}>Sophie Apple Nike+</Text>
                <View style={styles.connected_txt1Mas}>
                    <Text style={styles.connected_txt1}>Connected</Text>
                    <View style = {styles.container}>
                       <Switch
                          onValueChange = {()=>{
                            console.log('checked')
                          }}
                          value = {true}/>
                    </View>
                </View>
            </View>
      </View>

      </ScrollView>


      <View style={styles.footer}>
            <TouchableOpacity onPress={()=>this.onPressLogin(navigation )}>
              <View style={styles.save_view}>
                  <Text style={styles.save_btnTxt}>Save devices</Text>
              </View>

              <Image
                style={styles.save_btnImg}
                source={{ uri: 'buttonimg' }}
              />

            </TouchableOpacity>
        </View>

      </View>

  );
}
}
