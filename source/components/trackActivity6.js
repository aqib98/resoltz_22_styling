/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';
import { NavigationActions } from 'react-navigation';
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');


import styles from './styles.js'



export default class TrackActivity6 extends Component<{}> {
  constructor(props) {
  super(props);
  this.state = {
    width:0,
    height : 0
  };
}

  render() {
    const {navigation} = this.props
    const resizeMode = 'center';
    const text = 'I am some centered text';
    return (

      <View style={{
          backgroundColor: '#2d2e37',
          marginTop : 0,
          padding:0,
          height: Dimensions.get('window').height,
          width: Dimensions.get('window').width
          }}
      >
            <Text style={styles.upcomingWorkouts_heading}>
               trackActivity6
            </Text>
      </View>

    );
  }
}
