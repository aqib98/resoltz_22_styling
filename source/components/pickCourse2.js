/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  Picker
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import DatePicker from 'react-native-datepicker'
import {Slider} from 'react-native-elements'
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');



import styles from './styles.js'



export default class PickCourse2 extends Component<{}> {
  constructor(props) {
  super(props);
  this.state = {
    width:0,
    height : 0
  };
}

  render() {
  
    const {navigation} = this.props
    const resizeMode = 'center';
    const text = 'I am some centered text';
    return (

      <View style={{
          backgroundColor: '#2d2e37',
          marginTop : 0,
          padding:0,
          height: Dimensions.get('window').height,
          width: Dimensions.get('window').width
          }}
      >
            <Text style={styles.upcomingWorkouts_heading}>
               pickCourse2
            </Text>
            <Slider
                    style = {{width:100}}
                    step={1}
                    minimumValue={0}
                    maximumValue={2}
                    thumbTouchSize = {{width: 80, height: 80}}
                    thumbTintColor = 'orange'
                     />

                     <DatePicker

                  mode="date"
                  placeholder="select date"
                  format="YYYY-MM-DD"
                  minDate="2017-05-01"
                  maxDate="2020-06-01"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateInput:{
                      borderWidth: 0,
                    },
                    dateText:{
                      color: '#c7c8ca',
                      justifyContent: 'flex-start'
                    }
                  }}

                />

                <Picker>
                  <Picker.Item label="1 feet" value="1"/>
                  <Picker.Item label="2 feet" value="2"/>
                  <Picker.Item label="3 feet" value="3"/>
                  <Picker.Item label="4 feet" value="4"/>
                  <Picker.Item label="5 feet" value="5"/>
                  <Picker.Item label="6 feet" value="6"/>
                  <Picker.Item label="7 feet" value="7"/>
                  <Picker.Item label="8 feet" value="8"/>
                  <Picker.Item label="9 feet" value="9"/>
                  <Picker.Item label="10 feet" value="10"/>
                  <Picker.Item label="11 feet" value="11"/>
                  <Picker.Item label="12 feet" value="12"/>
                </Picker>


      </View>

    );
  }
}
