/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { NavigationActions } from 'react-navigation';
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

import SplashScreen from 'react-native-smart-splash-screen'

import styles from './styles.js'



export default class Home extends Component<{}> {
  constructor(props) {
  super(props);
  this.state = {
    width:0,
    height : 0
  };
}
componentDidMount () {
     //SplashScreen.close(SplashScreen.animationType.scale, 850, 500)
     // SplashScreen.close({
     //    animationType: SplashScreen.animationType.scale,
     //    duration: 850,
     //    delay: 2000,
     //
     // })

}


 onPressSignin = (navigate) => {
   const navigateAction = NavigationActions.navigate({

     routeName: 'SignIn',
     params: {'SignIn':'yes'},
     })
   navigate.dispatch(navigateAction)
 }

 onPressSignUp = (navigate) => {
   const navigateAction = NavigationActions.navigate({

     routeName: 'SignUp',
     params: {'SignUp':'yes'},

     })
   navigate.dispatch(navigateAction)
 }

  render() {
    const {navigation} = this.props
    const resizeMode = 'center';
    const text = 'I am some centered text';
    return (

      <View style={{

                backgroundColor: '#2d2e37',
                marginTop : 0,
                padding:0,
                height: Dimensions.get('window').height,
                width: Dimensions.get('window').width
              }}
            >
              <View
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  width: undefined,
                  height: undefined,
                }}
              >
                <Image
                  style={{

                    resizeMode: 'stretch',
                    height: Dimensions.get('window').height,
                    width: Dimensions.get('window').width,
                  }}
                  source={{ uri: 'signinbg' }}
                />
              </View>

              <View style = {{marginTop : '14%',justifyContent:'center',alignItems:'center'}}>
                <Image
                  style={{
                    resizeMode: 'contain',
                    width:140,
                    height:30,

                  }}
                  source={{ uri: 'signinlogo' }}
                />


              </View>
              <View style = {{marginTop : '86%',alignItems:'center',justifyContent:'center'}}>


                    <View style={{ alignItems:'center',justifyContent:'center'}}>
                      <View>
                        <Text style = {styles.textStyle}>Get inspired with our challenges quotes and friends workouts</Text>

                      </View>



                    </View>

              </View>
              <View style ={{ alignItems:'center',justifyContent:'center', position:'absolute', bottom:0, left:0, right:0}}>
                    <View>
                      <TouchableOpacity onPress={()=>this.onPressSignUp(navigation )}>
                          <Image
                            style={{
                              resizeMode: 'contain',
                              width:230,
                              height:80,
                            }}
                            source={{ uri: 'signupbtn' }}
                          />
                      </TouchableOpacity>
                    </View>
                    <View style ={{ alignItems:'center',justifyContent:'center'}}>
                    <Text style={styles.textStyle}>
                    <Text>I have an account </Text>
                    <Text style = {{fontWeight:'bold',color:'#fb620c'}} onPress={()=>this.onPressSignin(navigation )}>
                    Sign In
                    </Text>
                    </Text>

                    </View>
              </View>
            </View>

    );
  }
}
